/* S�ntese
    - Objetivo: Programa para simular o gerenciamento de mem�ria de um SO
    - Entradas: Tamanho da mem�ria, Tamanho da p�gina, Nome do programa, Tamanho do programa
    - Sa�das: Tabela (Gerenciador de tarefas), Representa��o Gr�fica
*/

/* Fun��es do programa
    - defineMemoria(int tamanho_memoria, int tamanho_p�gina)
    - int runPrograma(char * nome_do_programa, int tamanho_memoria_programa)
        * Deve retornar um "ID" do programa, para ser identificado posteriormente
    - killPrograma(int ID_do_programa)
    - gerenciadorDeTarefas(void)
        * Mostra uma tabela com os processos em execu��o
        * Ao final, mostra o total de mem�ria usada e total de mem�ria livre
    - runDefrag(void)
        * Essa fun��o est� respons�vel para "organizar" a mem�ria utilizada
        * Antes:    Mem�ria: |x|x| |x| | |
        * Depois:   Mem�ria: |x|x|x| | | |
*/

/* Emails para contato
    - matheus_oliveira30@hotmail.com
    - matheussbernardo@gmail.com
    - renata.sds09@gmail.com
*/

#include <stdio.h>
#include <stdlib.h>
/* Incluir header para gr�ficos */

/* Defini��o de um "programa" */
typedef struct processo
{
    char * nome_do_programa;
    int ID_programa;
    int tamanho_necessario;
} programa;

/* Defini��o da lista */
typedef struct node
{
    programa * processo_rodando;
    /* Deve ser NULL se n�o tiver ningu�m rodando */
    /* Caso tenha algum processo rodando, deve possuir um ponteiro com as informa��es do programa em execu��o*/

    /* Vari�veis para indicar o total de mem�ria por n� */
    int tamanho_utilizado;
    int tamanho_maximo;

    struct node * anterior;
    struct node * proximo;
} pagina_memoria;

/* Defini��o do cabe�alho */
typedef struct header
{
    /* Apontadores para in�cio e fim da mem�ria */
    pagina_memoria * inicio;
    pagina_memoria * fim;

    /* Vari�veis de uso da mem�ria */
    int total_utilizado, total_livre, total, particoes;

    /* Vari�vel para indicar a primeira p�gina livre */
    pagina_memoria * primeira_livre;
} header_memoria;

/* --------------------FIM DAS STRUCTS------------------------ */

/* Fun��o de gerar relacionados a Listas Encadeadas */
header_memoria * iniciarMemoria(void);
pagina_memoria * gerarNovaPagina(int tamanho_max);
void inserirFrente(header_memoria * head, int tamanho_pag);

/* Fun��es relacionadas a gerar processos */
pagina_memoria * getVazio(header_memoria * memoria);
void alocarNovoProcesso(header_memoria * memoria, char * nome, int ID, int tamanho);
programa * gerarNovoProcesso(char * nome, int ID, int tamanho);

/* Fun��es relacionadas � simula��o da mem�ria */
void definirMemoria(int tamanho_max, int tamanho_pag, header_memoria * memoria);

/* Fun��es de DUBUG */
void imprimirTamanhoMaximo(header_memoria * memoria);
void gerarTabelaPonteiros(header_memoria * memoria);
void gerarTabelaSimplesBlocos(header_memoria * memoria);

/* Fun��es de INTERFACE */
void lerString(char * vetor_char);
int leInt(void);
void limparTela(void);



/* ------------------FIM DAS PROTOTIPAGENS-------------------- */



/* Fun��o para gerar processo vazio */
programa * gerarNovoProcesso(char * nome, int ID, int tamanho)
{
    programa * novoPrograma = (programa*)malloc(sizeof(programa));

    novoPrograma->ID_programa = ID;
    novoPrograma->nome_do_programa = nome;
    novoPrograma->tamanho_necessario = tamanho;

    return novoPrograma;
}

/* Gera o processo (programa) e p�e ele em uma p�gina*/
void alocarNovoProcesso(header_memoria * memoria, char * nome, int ID, int tamanho)
{
    pagina_memoria * pagina_livre;
    int tamanho_total;

    tamanho_total = tamanho;

    /* Caso n�o exista mem�ria suficiente, a fun��o para aqui */
    if(tamanho > memoria->total_livre)
    {
        printf("ERRO ao gerar o processo! Nao ha memoria para esse novo processo.\n");
        return;
    }

    /* J� que h� mem�ria para o processo, ele ir� ocupar os blocos at� o "tamanho" ser zero */
    do
    {
        pagina_livre = getVazio(memoria);

        /* Faz a verifica��o se foi achado alguma p�gina */
        if(pagina_livre == NULL)
        {
            printf("ERRO ao gerar o processo! Nao foram encontradas paginas vazias para o processo.\n");
            return;
        }

        /* Gera o processo nesse bloco que foi encontrado */
        if(tamanho > pagina_livre->tamanho_maximo) /* Caso o tamanho seja maior que o m�ximo a ser alocado*/
        {
            pagina_livre->processo_rodando = gerarNovoProcesso(nome, ID, pagina_livre->tamanho_maximo);

            (pagina_livre->processo_rodando)->tamanho_necessario = tamanho_total; /* Informa a propriedade do processo do total que ele ocupa na mem�ria */
            pagina_livre->tamanho_utilizado = pagina_livre->tamanho_maximo; /* Informa o total utilizado no bloco */

            tamanho -= pagina_livre->tamanho_maximo;
        }
        else
        {
            pagina_livre->processo_rodando = gerarNovoProcesso(nome, ID, tamanho);

            (pagina_livre->processo_rodando)->tamanho_necessario = tamanho_total; /* Informa a propriedade do processo do total que ele ocupa na mem�ria */
            pagina_livre->tamanho_utilizado = tamanho; /* Informa o total utilizado no bloco */

            tamanho = 0;
        }

    } while(tamanho > 0);
}

/* Fun��o para pegar o primeiro n� vazio */
/* Retorna o endere�o do n� vazio - Ou - Retorna NULL se n�o achar ningu�m */
pagina_memoria * getVazio(header_memoria * memoria)
{
    pagina_memoria * contador;
    int controle, achou_vazio = 0;

    contador = memoria->inicio;

    /* Loop respons�vel por ficar aprocurando um bloco vazio */
    for(controle = 0; controle < memoria->particoes; controle++)
    {
        /* Caso esse seja vazio, o loop para */
        if(contador->processo_rodando == NULL)
        {
            achou_vazio = 1;
            break;
        }

        contador = contador->proximo;
    }

    if(achou_vazio == 1)
    {
        return contador;
    }
    else
    {
        return NULL;
    }

}


/* Fun��o para gerar uma p�gina vazia */
pagina_memoria * gerarNovaPagina(int tamanho_max)
{
    pagina_memoria * novaPagina = (pagina_memoria*)malloc(sizeof(pagina_memoria));

    novaPagina->anterior = NULL;
    novaPagina->proximo = NULL;

    novaPagina->processo_rodando = NULL;

    novaPagina->tamanho_utilizado = 0;
    novaPagina->tamanho_maximo = tamanho_max;

    return novaPagina;

}

/* Fun��o para inserir na frente */
void inserirFrente(header_memoria * head, int tamanho_pag)
{
    pagina_memoria * novoNo = gerarNovaPagina(tamanho_pag);

    if(head->inicio == NULL)
    {
        head->inicio = novoNo;
        head->fim = novoNo;

        (head->inicio)->proximo = head->fim;
        (head->inicio)->anterior = head->fim;

        (head->fim)->proximo = head->inicio;
        (head->fim)->anterior = head->inicio;
    }
    else
    {
        novoNo->proximo = head->inicio;
        novoNo->anterior = head->fim;

        head->inicio = novoNo;
        (head->fim)->proximo = head->inicio;
        (head->inicio)->anterior = head->fim;
    }

}

/* Fun��o para definir a mem�ria (Criar os blocos/p�ginas) */
void definirMemoria(int tamanho_max, int tamanho_pag, header_memoria * memoria)
{
    int contador, particoes;

    /* Define o n�mero de parti��es da mem�ria */
    if(tamanho_max % 2 == 0 && tamanho_max % tamanho_pag == 0)
    {
        particoes = tamanho_max / tamanho_pag;
    }
    else
    {
        printf("Erro - Memoria nao particionavel!");
        return;
    }

    /* Cria parti��es e a mem�ria */
    for (contador = 0; contador < particoes; contador++)
    {
        inserirFrente(memoria, tamanho_pag);
    }

    /* Salva as informa��es da mem�ria */
    memoria->particoes = particoes;
    memoria->total = tamanho_max;
    memoria->total_livre = tamanho_max;
    memoria->total_utilizado = 0;
}


/* Fun��o respons�vel por iniciar a mem�ria (cabe�alho) */
header_memoria * iniciarMemoria(void)
{
    header_memoria * novaMemoria = (header_memoria*)malloc(sizeof(header_memoria));

    novaMemoria->inicio = NULL;
    novaMemoria->fim = NULL;

    novaMemoria->primeira_livre = NULL;

    novaMemoria->total = 0;
    novaMemoria->total_livre = 0;
    novaMemoria->total_utilizado = 0;

    return (novaMemoria);
}

/* Imprime o tamanho m�ximo que cada bloco pode receber */
void imprimirTamanhoMaximo(header_memoria * memoria)
{
    pagina_memoria * contador = memoria->inicio;
    int controle;

    for(controle = 0; controle < memoria->particoes; controle++)
    {
        printf("Bloco #%d = %d\n", controle, contador->tamanho_maximo);
        contador = contador->proximo;
    }

}


/* DEBUG - Imprime os ponteiros dos programas alocados */
/* NULL se n�o estiver alocado - X se estiver alocado */
void gerarTabelaPonteiros(header_memoria * memoria)
{
    int contador;
    pagina_memoria * temp = memoria->inicio;
    programa * teste = temp->processo_rodando;


    for(contador = 0; contador < memoria->particoes; contador++)
    {
        printf("Bloco #%d - Endereco de memoria do programa: %p\n", contador, teste);
        temp = temp->proximo;
        teste = temp->processo_rodando;
    }
}

/* DEBUG - Fun��o para gerar uma tabela b�sica de todos os blocos*/
void gerarTabelaSimplesBlocos(header_memoria * memoria)
{
    int contador;
    pagina_memoria * bloco = memoria->inicio;
    programa * processo_alocado = bloco->processo_rodando;

    printf("\t\t\tTabela de TODOS os Blocos/Paginas\n\n");

    /* Loop para imprimir os itens da lista */
    for(contador = 0; contador < memoria->particoes; contador++)
    {
        /* Caso n�o tenha um programa alocado */
        if(processo_alocado == NULL)
        {
            printf("Bloco #%d - Vazio! \n", contador);
            bloco = bloco->proximo;
            processo_alocado = bloco->processo_rodando;
        }
        else
        {
            printf("Bloco #%d\nNome: %s - ID: %d\n", contador, processo_alocado->nome_do_programa, processo_alocado->ID_programa);
            printf("Endereco de memoria: %p\n", processo_alocado);
            printf("Tamanho Necessario: %d - Tamanho ocupado na pagina: %d\n", processo_alocado->tamanho_necessario, bloco->tamanho_utilizado);

            bloco = bloco->proximo;
            processo_alocado = bloco->processo_rodando;
        }

        putchar('\n');
    }
}

/* INTERFACE - Fun��o para pegar uma string usando getchar() */
void lerString(char * vetor_char)
{
    int contador = 0;
    int tamanho_vetor;

    /* Calcula o tamanho do vetor */
    tamanho_vetor = sizeof(vetor_char) / sizeof(char);

    /* Enquanto o caracter digitador for diferente do ENTER || Menor que o tamanho m�ximo do vetor */
    while((vetor_char[contador] = getchar()) != '\n')
    {
        if(contador < tamanho_vetor)
        {
          contador++;
        }
        else
        {
            vetor_char[contador] = '\0';
            break;
        }
    }
}

/* INTERFACE - Fun��o para ler inteiros*/
int leInt(void)
{
    int inteiro;

    do
    {
        scanf("%d", &inteiro);

        if(inteiro < 0)
        {
            printf("O valor digitado e invalido!\n");
        }

    } while (inteiro < 0);

    return inteiro;
}

/* INTERFACE - FUN��O TEMPOR�RIA - Fun��o para limpar a tela */
void limparTela(void)
{
    int contador;

    for(contador = 0; contador < 50; contador++)
    {
        putchar('\n');
    }
}


int main(void)
{
    char nome_programa[30];
    int id_programa, tamanho_programa;
    int memoria_max, memoria_pagina;
    int interacoes_user;
    header_memoria * head = iniciarMemoria();

    printf("Digite o tamanho maximo da memoria: ");
    memoria_max = leInt();

    printf("Digite o tamanho da pagina: ");
    memoria_pagina = leInt();

    definirMemoria(memoria_max, memoria_pagina, head);


    printf("\nDigite a quantidade de interacoes desejadas: ");
    interacoes_user = leInt();

    while (interacoes_user > 0)
    {
        putchar('\n');

        printf("Digite o nome do processo: ");
        getchar();
        lerString(nome_programa);

        printf("Digite a ID do programa: ");
        id_programa = leInt();
        getchar();

        printf("Digite o tamanho que o programa ocupara: ");
        tamanho_programa = leInt();
        getchar();

        alocarNovoProcesso(head, nome_programa, id_programa, tamanho_programa);

        interacoes_user--;
    }

    printf("\n\n");
    gerarTabelaSimplesBlocos(head);

    return 0;
}
